This is a simple "Nginx - PHP7 FPM - MySQL" docker environment using docker-compose.

# General:

This is a simple way to start a small PHP project with Nginx and MySql. 

You just need to run **docker-compose up** and then you will have a local environment to play with your PHP code. 

- You should add your code in **./code** folder
- Database data will be in **./data** folder 
- To add more php extensions edit **./etc/php/Dockerfile**
- To test your installation:
    - http://localhost/
    - http://localhost/phpinfo.php
    
# Install prerequisites:

For now, this project has been mainly created for Unix (Linux/MacOS). Perhaps it could work on Windows.


All requisites should be available for your distribution. The most important are :

- [Git](https://git-scm.com/downloads)
- [Docker](https://docs.docker.com/engine/installation/)
- [Docker Compose](https://docs.docker.com/compose/install/)


Check if docker-compose is already installed by entering the following command :

which docker-compose

Check Docker Compose compatibility :

- [Compose file version 3 reference](https://docs.docker.com/compose/compose-file/)

# Commands:

**Docker-compose commands:**

```sh
$ docker-compose help
``` 

**Stop and remove containers, networks, images, and volumes:**

```sh
$ docker-compose down
```

**Build or rebuild services:**

```sh
$ docker-compose build
```

**Run a one-off command:**

```sh
$ docker-compose run
```

**Run in background:** 

```sh
$ docker-compose run - d
```

**Logs:**

```sh
$ docker-compose logs mysql

$ docker-compose logs php

$ docker-compose logs nginx 
```

# Sources: 

https://docs.docker.com/compose/gettingstarted/

https://docs.docker.com/compose/compose-file/

http://geekyplatypus.com/dockerise-your-php-application-with-nginx-and-php7-fpm/

http://geekyplatypus.com/making-your-dockerised-php-application-even-better/

https://github.com/nanoninja/docker-nginx-php-mysql

https://mindthecode.com/setting-up-laravel-with-docker-compose/

https://github.com/docker-library/php/issues/309
